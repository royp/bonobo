import java.util.Locale._

import DataGenerator._
import org.joda.money.CurrencyUnit._

import scala.util.matching.Regex
import scala.collection.JavaConverters._
import scala.xml.{Text, TopScope, Null, Elem}

object TestRequestData {

  val countries = List("CZ", "PL")
  val currencies = registeredCurrencies().asScala
    .filter(!_.isPseudoCurrency).map(_.getSymbol)
  val offices = getAvailableLocales.map(_.getCountry).filter("[A-Z]{2}".r matches)
  val baseId = create("N4")

  def createRequests = Stream.continually(createRequest(f"$baseId$i%06d"))

  /** @return a &lt;PvdRequest&gt; with random content */
  def createRequest(messageId: String) = {
    val fields = Map(
      "MessageId" -> messageId,
      "OfficeId" -> (choose(offices)+create("N1")),
      "OriginalCurrency" -> choose(currencies),
      "BeneficiaryBankNcc" -> create("AN11"),
      "BeneficiaryBankId" -> "",
      "BeneficiaryBankIdType" -> create("A2"),
      "BeneficiaryBankCountry" -> choose(countries),
      "BeneficiaryAccountId" -> (choose(offices)+create("N2AN4N14")),
      "InLocalCurrency" -> "")

    <val:PvdRequest xmlns:val="http://com.citi.com/2015/01/autofx">
      {fields xml}
    </val:PvdRequest>
  }

  implicit class Reggaid(r: Regex) {
    def matches(s: String) = s match { case r() => true; case _ => false}
  }

  implicit class Xmlaid(fields: Map[String, String]) {
    /** @return a list of XML elements created from the map's key/value pairs,
      * where the element name is the key and the element text, the value */
    def xml = fields.map { entry =>
      val (key, value) = entry
      Elem("val", key, Null, TopScope, minimizeEmpty=true, Text(value))
    }
  }

}
